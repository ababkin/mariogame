﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using UnityEngine.SceneManagement;

public class AdManager : MonoBehaviour, IRewardedVideoAdListener {
    bool coins = false;
    bool lives = false;
    // Use this for initialization
    void Start() {


        string appKey = "1e5d9769a127c06a4f0806e797badf91e6756a85e4a1e381";
        Appodeal.disableLocationPermissionCheck();
        Appodeal.setTesting(true);
        Appodeal.initialize(appKey, Appodeal.BANNER | Appodeal.INTERSTITIAL | Appodeal.REWARDED_VIDEO);
        Appodeal.setRewardedVideoCallbacks(this);
     
    }
	/// <summary>
    /// Other ads implementation
    /// </summary>
  /* public void ShowBaner() 
    {
        if(Appodeal.isLoaded(Appodeal.BANNER))
        {
            Appodeal.show(Appodeal.BANNER_TOP);
        }
    }


    public void HideBanner()
    {
        Appodeal.hide(Appodeal.BANNER);
    }

    public void ShowInerstitial()
    {
        if(Appodeal.isLoaded(Appodeal.INTERSTITIAL))
        {
            Appodeal.show(Appodeal.INTERSTITIAL);
        }
    }
    */
    public void ShowRewarded()
    {
        if(Appodeal.isLoaded(Appodeal.REWARDED_VIDEO))
        {
            Appodeal.show(Appodeal.REWARDED_VIDEO);
            coins = true;
            
        }
    }

    public void WatchAdToGetLives()
    {
        if (Appodeal.isLoaded(Appodeal.REWARDED_VIDEO))
        {
            Appodeal.show(Appodeal.REWARDED_VIDEO);
            lives = true;
        }
    }


    public void onRewardedVideoLoaded()  {  }

    public void onRewardedVideoFailedToLoad()  {  }

    public void onRewardedVideoShown() { }

     public void onRewardedVideoClosed() { }

    public void onRewardedVideoFinished(int amount, string name)
    {
        if (coins)
        {
            PlayerPrefs.SetInt(GlobalValue.Coins, PlayerPrefs.GetInt(GlobalValue.Coins) + 50);
            coins = false;
        }

        if(lives)
        {
            PlayerPrefs.SetInt(GlobalValue.Lives,3);
            lives = false;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
      
    }




}
